# Скрипты обслуживания

### Скрипт обновления БД движка до последней версии

Что бы обновить движок до последней версии необходимо запустить команду `./manage.py sshop_update <version>`, где `<version>` - номер текущей версии движка. Скрипт выполнит все необходимые миграции в БД.

### Скрипт импорта из 1С

Для запуска импорта из 1С, необходимо запустить команду `./manage.py import_from_1c`. Скрипт по-очереди запустит каждое задание на выполнение. Более детально описано в [Интеграция с 1С - API импорта](1c_import_api.md)
# Продукты

### Система статусов товаров

    class ProductStatus(models.Model):
        name = models.CharField(_(u"Name"), max_length=70)
        css_class = models.CharField(_(u"Css class"), max_length=70, blank=True)
        description = models.TextField(_(u"Description"), blank=True)
        order = models.IntegerField(default=10)
        show_buy_button = models.BooleanField(_(u"Show buy button"), default=True)
        show_ask_button = models.BooleanField(_(u"Show ask button"), default=True)
        is_visible = models.BooleanField(_(u"Is visible"), default=True)
        is_searchable = models.BooleanField(_(u"Is searchable"), default=True)

* `name` - название статуса
* `css_class` - опциональная возможность задать класс для метки
* `description` - подробное текстовое описание статуса, будет отображаться при нажатии на метку.
* `show_buy_button` - критерий доступности товара для покупок
* `show_ask_button` - критерий показа кнопки "Задать вопрос"
* `is_visible` - критерий видимости товара в списках категорий
* `is_searchable` - критерий доступности товара в поиске

Дана система позволяет создавать и описывать правила для статусов продуктов. Пример статуса `В наличии` который доступен для поиска, покупки, возможность задать вопрос и с меткой `success`: ProductStatus.objects.create(name=u"В наличии", css_class='label-success').


### Система сортировки товаров

    class SortType(models.Model):
        name = models.CharField(_(u"Name"), max_length=70)
        order = models.IntegerField(default=0)
        sortable_fields = models.TextField(_(u"Sortable fields"), blank=True)

* `name` - название способа сортировки
* `sortable_fields` - имена полей для сортировки через запятую (согласно правилам функции `order_by`)

Дана система позволяет создавать и указывать сортировки с админки, указывая поля сортировки. Примеры поля `sortable_fields`:

* price
* -price
* name
* -name
* productstatus.order, -effective_price
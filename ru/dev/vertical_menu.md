# Вертикальное меню

Это отдельный плагин, который позволят вставить выпадающее меню как на [amazon.com](http://www.amazon.com).
[Скриншот](vertical_menu.png).

Этот плагин находится в корне магазина в папке `vertical_menu`.

#### Подключение плагина к магазину

Подключается плагин как обычная Django-аппликация - в настройках проекта (основных или локальных) в `INSTALLED_APPS` нужно добавить строку `vertical_menu` ([дополнительно здесь](https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-INSTALLED_APPS)).

Потом, нужно добавить меню в магазине. Для этого необходимо вставить специальный Django-тег в основном шаблоне проекта (обычно это файл `sshop/basic_theme/templates/lfs/base.html`). Сперва в начале шаблона необходимо подключить модуль с тегом `{% load vertical_menu_tags %}`, потом в нужном месте вставить тег `{% horizonral_menu %}`, который генерирует и вставляет меню в указанном месте.

#### Редактирование плагина

Все файлы плагина лежат в папке `vertical_menu`, плюс необходим jQuery (подключается глобально).

Плагин имеет следующую структуру:

    vertical_menu /
        static /
            css /
                vertical_menu.css - файл стилей
        templates /
            vertical_menu /
                vertical_menu.html - шаблон меню
        templatetags /
            __init__.py
            vertical_menu_tags.py - шаблонные теги

В файле `vertical_menu_tags.py` написан шаблонный тег, который рендерит меню используя шаблон `vertical_menu.html`. В `vertical_menu.css` находится стили даного меню. Этот файл стилей необходимо подключить в базовый шаблон проекта (`sshop/basic_theme/templates/lfs/base.html`).

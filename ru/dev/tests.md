# Тесты

#### Указания приложений для тестирования

Для того, что бы lettuce не пытался запускать тески с каждого приложения в проекте, в `settings.py` добавлена переменная `LETTUCE_APPS`, в которой явно указываются приложения для которых запускать тесты.

#### Создание фикстуры с тестовыми даными

Для создания фикстуры нужно использовать следующую команду:

    ./manage.py dumpdata --natural --exclude=contenttypes --exclude=auth.Permission --exclude=portlets.PortletRegistration > initial_data.json

Она создает дамп всей БД в файл `initial_data.json`. Если возникнут ошибки на подобии:

    IntegrityError: Could not load portlets.PortletRegistration(pk=13): duplicate key value violates unique constraint "portlets_portletregistration_type_key"
    DETAIL:  Key (type)=(carouselportlet) already exists.

то, нужно исключить проблемную модель из дампа добавив к команде следующий ключ:

    --exclude=portlets.PortletRegistration

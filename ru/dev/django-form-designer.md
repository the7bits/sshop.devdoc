#Form designer

Для того, чтобы включить отображение кастомных форм, необходимо загрузить templatetags в начале шаблона строкой

    {% load formdesigner %}

После этого можно использовать тег

    {% render_form 'form-name' %}

где 'form-name' - имя формы, указанное при создании

Шаблоны для рендеринга формы указываются в админке, по умолчанию это form_designer/templates/html/formdefinition/detail.html
# Подсистема медиатора

### Запуск события

`mediator.publish(<event>, <args>...)` - запустит событие `<event>` с аргументами `<args>`. 

Пример: mediator.publish('change_slots_width', 'span3', 'span6', 'span9') - запустит событие `change_slots_width` с аргументами `'span3', 'span6', 'span9'.

### События

В магазине доступны события:

* `list_page_mode` - тип отображения товаров измен на список;
* `table_page_mode` - тип отображения товаров измен на таблица;
* `products_sorting_price_upward` - выбрана сортировка товаров по возрастанию цены;
* `products_sorting_price_downward` - выбрана сортировка товаров по убиванию цены;
* `products_sorting_name_upward` - выбрана сортировка товаров по алфавиту;
* `products_sorting_name_downward` - выбрана сортировка товаров обратно алфавиту;
* `change_amount_cart_item` - изменено количество товаров в корзине;
* `remove_cart_item` - удален товар с корзины;
* `filter_update` - фильтр страницы товаров изменен;
* `search` - нажата кнопка поиска;
* `start_update_checkout` - началось обновление страницы заказа;
* `success_update_checkout` - обновление страницы заказа завершилось успешно;
* `add_to_cart` - добавлен товар в корзину;
* `submit_order` - сделан заказ.
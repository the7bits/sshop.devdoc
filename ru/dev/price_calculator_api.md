# Калькуляторы цен - API

API калькуляторов цен задается двумя классами в модуле `lfs/plugins.py`.

## Базовые классы

### class lfs.plugin.PriceCalculator(object)

Описывает алгоритм расчета цены товара. Данный класс являет собой фильтр который находится между ценой на товар хранящейся в БД и той ценой которая будет отображена на сайте. Он не предназначен для использования напрямую, а служит для наследования классов которые уже и будут описывать калькуляторы цен.

Атрибуты:

* `request` - объект класса [HttpRequest](https://docs.djangoproject.com/en/1.4/ref/request-response/#httprequest-objects) содержащий мета информацию о текущем запросе;
* `product` - объект класса `lfs.catalog.models.Product` указывающий на товар для которого производится расчет цены.

Методы:

* `__init__ (request, product, **kwargs)` - конструктор, при инициализации объекта присваивает значения переданные в качестве аргументов вышеприведенным аттрибутам.
    - `request` - объект класса [HttpRequest](https://docs.djangoproject.com/en/1.4/ref/request-response/#httprequest-objects) содержащий мета информацию о текущем запросе;
    - `product` - объект класса `lfs.catalog.models.Product` указывающий на товар для которого производится расчет цены.
* `get_price(with_properties=True)` - возвращает цену товара (с учетом возможной распродажи).
    - `with_properties=True` - если `True`, то для товаров с вариантами конечная цена будет рассчитана с учетом дефолтного варианта.
* `get_standard_price(with_properties=True)` - возвращает базовую цену товара (без учета распродажи).
    - `with_properties=True` - если `True`, то для товаров с вариантами конечная цена будет рассчитана с учетом дефолтного варианта.
* `get_for_sale_price(with_properties=True)` - возвращает цену товара с учетом распродажи.
    - `with_properties=True` - если `True`, то для товаров с вариантами конечная цена будет рассчитана с учетом дефолтного варианта.
* `get_base_packing_price(with_properties=True)` - возвращает цену товара с учетом упаковки, рассчитавается как `цена товара * количество единиц товара в упаковке`.
    - `with_properties=True` - если `True`, то для товаров с вариантами конечная цена будет рассчитана с учетом дефолтного варианта.


### class lfs.plugin.ShippingMethodPriceCalculator(object)

Описывает алгоритм расчета цены доставки. Данный класс являет собой фильтр который находится между ценой на доставку указанную в настройках конкретного типа доставки и окончателшьной ценой доставки фигурирующей при расчете стоимости заказа. Он не предназначен для использования напрямую, а служит для наследования классов описывающих логику расчета стоимости если необходим сложный алгоритм.

Атрибуты:

* `request` - объект класса [HttpRequest](https://docs.djangoproject.com/en/1.4/ref/request-response/#httprequest-objects) содержащий мета информацию о текущем запросе;
* `shipping_method` - объект класса `lfs.shipping.models.ShippingMethod` указывающий на выбранный способ доставки.

Методы:

* `__init__ (request, product, shipping_method)` - конструктор, при инициализации объекта присваивает значения переданные в качестве аргументов вышеприведенным аттрибутам.
    - `request` - объект класса [HttpRequest](https://docs.djangoproject.com/en/1.4/ref/request-response/#httprequest-objects) содержащий мета информацию о текущем запросе;
    - `shipping_method` - объект класса `lfs.shipping.models.ShippingMethod` указывающий на выбранный способ доставки.
* `get_price()` - возвращает цену доставки, по умолчанию возвращает значение атрибута `price` объекта хранящегося в аттрибуте `shipping_method`.

## Настройки

Для использования калькуляторов цен, необходимо чтобы они были указаны в файле `settings.py`. Чтобы калькулятор цен стал доступен для выбора в интерфейсе управления магазином, он должен быть явно пожключен в настройках. Ниже описаны конкретные параметры.

### LFS_PRICE_CALCULATORS 

Описывает активные калькуляторы цен. Значение по умолчанию:

    LFS_PRICE_CALCULATORS = [
        ['lfs.default_price.DefaultPriceCalculator', ugettext(u'Default price calculator')],
    ]

### LFS_SHIPPING_METHOD_PRICE_CALCULATORS

Описывает активные калькуляторы цен доставок. Значение по умолчанию:

    LFS_SHIPPING_METHOD_PRICE_CALCULATORS = [
        ['lfs.default_price.DefaultShippingPriceCalculator', ugettext(u'Default shipping price calculator')],
    ]




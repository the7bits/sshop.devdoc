# Категории

#### Поле `extras`

Это поле предназначено для хранения дополнительной информации про категорию. Поле имеет тип `JSONField` и все данные в нем хранятся в формате [json](http://json.org/json-ru.html). Заполнить поле можно в админке магазина, вкладка `Данные`.

##### Использование в шалонах

Пусть поле `extras` содержит следующие данные:

    {
        "parametr_1": "val1",
        "parametr_2": 2
    }

В шаблон передаем категорию в переменной `category` и используем:

    {{ category.extras.parametr_1 }}
    {{ category.extras.parametr_2 }}
